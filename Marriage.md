# Marriage and friend relationship

In dotted (- - -) edge, you have friend relationship, in directed (-->) edge you have adoption relationship, and in normal (---) edge, you have marriage relationship between people of the inter-insa discord server. In strong edge, you have IRL relationship.

```mermaid
graph TD;
  classDef CVL fill:#9444ff;
  classDef Espion fill:#979c9f;
  classDef Lyon fill:#f1c40f;
  classDef Rennes fill:#ff7d00;
  classDef Rouen fill:#00a5ff;
  classDef Toulouse fill:#ff0000;
  classDef HDF fill:#fc9eff;
  classDef Strasbourg fill:#2ecc71
  classDef EuroMed fill:#e91e63;
  classDef Emote fill:#f47fff;

  A([Zo]):::Espion---B([Sato]):::Espion;
  A---C([Mamine]):::Rouen;
  A---D([Laura]):::Toulouse;
  A---E([Tyney]):::Toulouse;
  F([Trino]):::CVL-->C;
  C---E;
  D---E;
  B---E;
  E===G([Kivero]):::Espion;
  E-->P([Stel]):::Toulouse;
  B-->P;
    M([Meta]):::Toulouse---B;
  I([Mople]):::Toulouse===J([Catnip]):::Rennes;
  J-->H([Scipi0]):::Rennes;
  I-->H;
  K([Jeanne]):::Rennes===L([Keyro]):::Espion;
  N([Ae-Chan]):::Espion===O([Aizen]):::Toulouse;
  E-->eA([Lapignon]):::Emote;
  J-->R([Tonysan]):::Rennes;
  I-->R;
  A---S([Dim's]):::Toulouse;
```
